import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import './App.css';
import SimpleCounter from './SimpleCounter';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="simplecounter">
            <Route index element={<SimpleCounter />} />
          </Route>
          {/* <Route path="thermostat">
            <Route index element={<Thermostat />} />
          </Route> */}
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
