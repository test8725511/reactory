import React, { Component } from 'react';
import './App.css';


class SimpleCounter extends Component {
    constructor(props) {
        super(props);
        this.state = {
        count: 0
        }
    }

    increment = () => {
        this.setState({count: this.state.count + 1});
    }
    bigincrement = () => {
        this.setState({count: this.state.count + 10});
    }
    decrement = () => {
        this.setState({count: this.state.count - 1});
    }
    bigdecrement = () => {
        this.setState({count: this.state.count - 10});
    }

    render() {
        return (
        <div className="App">
            <button onClick={this.increment} className="counter">+</button>
            <button onClick={this.bigincrement} className="counter">++</button>
            <h1>{this.state.count}
            </h1>
            <button onClick={this.decrement} className="counter">-</button>
            <button onClick={this.bigdecrement} className="counter">--</button>
        </div>
        );
    }
}

export default SimpleCounter;
