import logo from './logo.svg'

function MainPage() {
    return (
        <div className='main'>
            <header className="App-header">
                <div className='content'>
                    <h1 className='display-5 fw-bold'>Welcome to the REACTory</h1>
                </div>
                <div>
                    <img src={logo} className="App-logo" alt="logo" />
                    <p className='d-flex align-items-center'>Showcasing a variety of React Projects!</p>
                </div>
            </header>
        </div>
    );
}


export default MainPage;
