import React, { Component } from 'react';
import './App.css';


class SimpleCounter extends Component {
    constructor(props) {
        super(props);
        this.state = {
        count: 0
        }
    }

    increment = () => {
        this.setState({count: this.state.count + 1});
    }
    bigincrement = () => {
        this.setState({count: this.state.count + 10});
    }
    megaincrement = () => {
        this.setState({count: this.state.count + 50});
    }
    decrement = () => {
        this.setState({count: this.state.count - 1});
    }
    bigdecrement = () => {
        this.setState({count: this.state.count - 10});
    }
    megadecrement = () => {
        this.setState({count: this.state.count - 50});
    }

    render() {
        return (
        <div className="counter">
            <button onClick={this.increment} className="smposcounter">+</button>
            <button onClick={this.bigincrement} className="medposcounter">++</button>
            <button onClick={this.megaincrement} className="lgposcounter">+++</button>
            <h1>{this.state.count}
            </h1>
            <button onClick={this.decrement} className="smnegcounter">-</button>
            <button onClick={this.bigdecrement} className="mednegcounter">--</button>
            <button onClick={this.megadecrement} className="lgnegcounter">---</button>
        </div>
        );
    }
}

export default SimpleCounter;
