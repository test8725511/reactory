import React, {useState} from 'react';
import './Thermostat.css';


const Thermostat = () => {
    const [temperatureValue, setTemperatureValue] = useState(75);
    const [temperatureColor, setTemperatureColor] = useState("neutral");

    const increaseTemperature = () => {
        if(temperatureValue === 88) return(alert("That's too hot!"));
        const newTemperature = temperatureValue + 1;
        if(newTemperature >= 76){
            setTemperatureColor('hot')
        }
        setTemperatureValue(newTemperature);
    };

    const decreaseTemperature = () => {
        if(temperatureValue === 60) return(alert("That's too cold!"));
        const newTemperature = temperatureValue - 1;
        if(newTemperature < 76){
            setTemperatureColor('cold')
        }
        setTemperatureValue(newTemperature);
    };

    return (
        <div className="thermostat">
            <div className="app-container">
                <div className="temperature-display-container">
                    <div className={`temperature-display ${temperatureColor}`}>{temperatureValue}°F</div>
                </div>
                <div className="button-container">
                    <button onClick={() => increaseTemperature()}>+</button>
                    <button onClick={() => decreaseTemperature()}>-</button>
                </div>
            </div>
        </div>
    )
}

export default Thermostat
